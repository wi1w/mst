#!/bin/bash


### mst-mount -- Mount an USB storage for an MST user.

# Copyright (C) 2018 Artyom V. Poptsov <poptsov.artyom@gmail.com>
#
# This file is part of MST.
#
# MST is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# MST is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MST.  If not, see <http://www.gnu.org/licenses/>.

### DEBUG

# exec 1> /tmp/u.log
# exec 2>&1
# set -x

###

get_user_by_display_number() {
    local display_number="$1"
    local user=$(who | grep "(:$display_number)" \
		     | sed -re 's/^([^ ]+) +:[0-9]+.*/\1/g')
    echo "$user"
}

make_mount_dir() {
    local mount_dir="$1"
    local user="$2"
    [ -e "$mount_dir" ] || mkdir "$mount_dir"
    sudo chown -R "$user:" "$mount_dir"
}

### Entry point.

main() {
    local device="$1"
    local display_number="$2"
    local user=$(get_user_by_display_number "$display_number")
    local user_id=$(id --user "$user")
    local group_id=$(id --group "$user")
    local mount_dir="/media/${user}_${display_number}"
    make_mount_dir "$mount_dir" "$user"

    sudo /bin/mount -o users,noauto,umask=0022,uid="$user_id",gid="$group_id" \
	 "$device" "$mount_dir"

    while [ $(df | grep "$mount_dir" -c ) -gt 0 ]; do
	  sleep 1;
    done
}

main $*

### mst-mount ends here.
